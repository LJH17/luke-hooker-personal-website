import Vue from 'vue'
import './plugins/helpers'
import './plugins/vuetify'
import App from './App.vue'
import AppPlugin from '@/plugins/app'
import router from './router'
import store from './store'
import '@/assets/stylus/main.styl'
import 'roboto-fontface/css/roboto/roboto-fontface.css'
import 'material-design-icons-iconfont/dist/material-design-icons.css'
import './registerServiceWorker'

Vue.use(AppPlugin)
Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
