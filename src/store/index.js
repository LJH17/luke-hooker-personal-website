import Vue from 'vue'
import Vuex from 'vuex'

import actions from './actions'

import apps from './modules/apps'

Vue.use(Vuex)

const debug = process.env.NODE_ENV !== 'production'

const modules = {
    apps
}

export default new Vuex.Store({
    actions,
    modules,
    strict: debug
})
