import { first } from 'underscore'

import * as mutationTypes from './mutation-types'
import api from '@/store/api'
import { leagueOptions } from '@/store/static/fpl/leagueOptions'

const namespaced = true

const initialState = () => ({
    leagueOptions,
    leagueData: {
        league: first(leagueOptions),
        standings: {
            results: []
        }
    },
    leagueDataError: '',
    leagueDataLoading: false
})

const state = initialState()

const getters = {
    leagueOptions: state => state.leagueOptions,
    leagueData: state => state.leagueData,
    selectedLeague: (state, getters) => getters.leagueData.league,
    leagueStandings: (state, getters) => getters.leagueData.standings.results,
    leagueError: state => state.leagueDataError,
    leagueLoading: state => state.leagueDataLoading
}

const actions = {
    resetFpl: ({ commit }) => commit(mutationTypes.RESET_FPL),
    getLeagueData: ({ commit, dispatch }, id = first(leagueOptions).id) => {
        dispatch('setLeagueDataLoading')
        dispatch('resetLeagueDataError')
        api.getLeagueData(id)
            .then((data) => {
                dispatch('setLeagueData', data)
            })
            .catch((error) => {
                // TODO: refactor to use SET_LEAGUE_ERROR as string type
                dispatch('setLeagueDataError', error.message ? error.message : 'Unable to get league data. Please try again.')
            })
            .then(() => dispatch('setLeagueDataLoading', false))
    },
    setSelectedLeague: ({ commit }, data) => commit(mutationTypes.SET_SELECTED_LEAGUE, data),
    setLeagueData: ({ commit, dispatch }, data = initialState().leagueData) => {
        dispatch('resetLeagueDataError')
        commit(mutationTypes.SET_LEAGUE_DATA, data)
    },
    setLeagueDataLoading: ({ commit }, data = true) => commit(mutationTypes.SET_LEAGUE_LOADING, data),
    setLeagueDataError: ({ commit }, data = 'An unexpected error occurred') => commit(mutationTypes.SET_LEAGUE_ERROR, data),
    resetLeagueDataError: ({ commit }) => commit(mutationTypes.RESET_LEAGUE_ERROR)
}

const mutations = {
    [mutationTypes.RESET_FPL] (state) {
        Object.assign(state, initialState())
    },
    [mutationTypes.SET_SELECTED_LEAGUE] (state, data) {
        state.leagueData.league = data
    },
    [mutationTypes.SET_LEAGUE_DATA] (state, data) {
        state.leagueData = data
    },
    [mutationTypes.SET_LEAGUE_ERROR] (state, data) {
        state.leagueDataError = data
    },
    [mutationTypes.RESET_LEAGUE_ERROR] (state) {
        state.leagueDataError = initialState().leagueDataError
    },
    [mutationTypes.SET_LEAGUE_LOADING] (state, data) {
        state.leagueDataLoading = data
    }
}

export default {
    namespaced,
    state,
    actions,
    getters,
    mutations
}
