import * as mutationTypes from './mutation-types'
import * as sheetNames from '@/store/static/tabletop/sheet-names'

const namespaced = true

const defaultSheet = {
    name: '',
    columnNames: [],
    elements: []
}

const initialState = () => ({
    year: 2021,
    spreadsheet: {
        [sheetNames.STANDINGS]: defaultSheet,
        [sheetNames.MONTH_TEMPLATE]: defaultSheet,
        [sheetNames.SCORING]: defaultSheet,
        [sheetNames.JANUARY]: defaultSheet,
        [sheetNames.FEBRUARY]: defaultSheet,
        [sheetNames.MARCH]: defaultSheet
    },
    spreadsheetError: '',
    spreadsheetLoading: false
})

const state = initialState()

const getters = {
    year: state => state.year,
    spreadsheet: state => state.spreadsheet,
    spreadsheetByName: (state, getters) => (name) => {
        return _.pick(getters.spreadsheet[name], _.keys(defaultSheet))
    },
    spreadsheetError: state => state.spreadsheetError,
    spreadsheetLoading: state => state.spreadsheetLoading,
    standingsFormatted: (state, getters) => {
        const standingsKeys = {
            position: 'position',
            player: 'player',
            points: 'points',
            difference: 'difference'
        }
        const standingsHeaders = [
            {
                text: 'Position',
                value: standingsKeys.position,
                align: 'center',
                sortable: true
            },
            {
                text: 'Player',
                value: standingsKeys.player,
                align: 'center',
                sortable: true
            },
            {
                text: 'Points',
                value: standingsKeys.points,
                align: 'center',
                sortable: true
            },
            {
                text: 'Dif. to Next Position',
                value: standingsKeys.difference,
                align: 'center',
                sortable: true
            }
        ]

        const standingsSpreadsheet = getters.spreadsheetByName(sheetNames.STANDINGS)
        if (!standingsSpreadsheet.elements) {
            return {
                headers: standingsHeaders,
                items: []
            }
        }

        const startingIndex = 4
        const numberOfPlayers = 5
        const standingsElements = standingsSpreadsheet.elements.slice(startingIndex, startingIndex + numberOfPlayers)
        const standingsItems = _.map(standingsElements, (el) => ({
            [standingsKeys.position]: el.GameStat,
            [standingsKeys.player]: el[getters.year],
            [standingsKeys.points]: el['Total Points'],
            [standingsKeys.difference]: el.Points
        }))

        return {
            title: 'Standings',
            headers: standingsHeaders,
            items: standingsItems,
            itemKey: standingsKeys.position
        }
    },
    scoringFormatted: (state, getters) => {
        const tableKeys = {
            numPlayers: 'No Players',
            five: '5',
            four: '4',
            three: '3'
        }
        const headers = _.map(tableKeys, (value) => ({
            text: value,
            value,
            align: 'center',
            sortable: false
        }))

        const spreadsheet = getters.spreadsheetByName(sheetNames.SCORING)
        if (!spreadsheet.elements) {
            return {
                headers,
                items: []
            }
        }

        const startingIndex = 0
        const length = 8
        const items = spreadsheet.elements.slice(startingIndex, startingIndex + length)

        return {
            title: 'Scoring System',
            headers,
            items,
            itemKey: tableKeys.numPlayers
        }
    },
    statsFormatted: (state, getters) => {
        const tableKeys = {
            player: 'Player',
            code: 'Code',
            totalPoints: 'Total Points',
            points: 'Points',
            basicPoints: 'Basic Points',
            wins: 'Wins',
            losses: 'Losses',
            missedWeeks: 'Missed Wks',
            played: 'Played',
            pointsMean: 'Points Mean',
            difficultyMean: 'Difficulty Mean'
        }
        const headers = _.map(tableKeys, (value) => ({
            text: value,
            value,
            align: 'center',
            sortable: true
        }))

        const tableData = {
            title: 'Stats',
            itemKey: tableKeys.code
        }
        const spreadsheet = getters.spreadsheetByName(sheetNames.STANDINGS)
        if (!spreadsheet.elements) {
            return {
                ...tableData,
                headers,
                items: []
            }
        }

        const startingIndex = 13
        const length = 5
        const elements = spreadsheet.elements.slice(startingIndex, startingIndex + length)

        const items = _.map(elements, (el) => ({
            ...el,
            [tableKeys.code]: el[getters.year],
            [tableKeys.player]: el.GameStat
        }))

        return {
            ...tableData,
            headers,
            items
        }
    },
    monthStatsFormatted: (state, getters) => (name) => {
        const tableKeys = {
            name: 'Name',
            points: 'Points',
            wins: 'Wins',
            losses: 'Losses',
            missedWeeks: 'Missed Weeks',
            played: 'Played',
            pointsAverage: 'Points Avg',
            meanDifficulty: 'Mean Difficulty'
        }
        const headers = _.map(tableKeys, (value) => ({
            text: value,
            value,
            align: 'center',
            sortable: true
        }))
        const tableData = {
            title: 'Overview',
            itemKey: tableKeys.name
        }

        const spreadsheet = getters.spreadsheetByName(name)
        if (!spreadsheet.elements) {
            return {
                ...tableData,
                headers,
                items: []
            }
        }

        const startingIndex = 2
        const length = 5
        const elements = spreadsheet.elements.slice(startingIndex, startingIndex + length)

        const items = _.map(elements, (el) => ({
            ...el,
            [tableKeys.name]: el[name]
        }))

        return {
            ...tableData,
            headers,
            items
        }
    },
    monthGameLogFormatted: (state, getters) => (name) => {
        const tableKeys = {
            name: 'Game:',
            game1: 'Game 1',
            game2: 'Game 2',
            game3: 'Game 3',
            game4: 'Game 4',
            game5: 'Game 5',
            game6: 'Game 6',
            game7: 'Game 7',
            game8: 'Game 8',
            game9: 'Game 9',
            game10: 'Game 10',
            totals: 'Totals'
        }
        const headers = _.map(tableKeys, (value) => ({
            text: value,
            value,
            align: 'center',
            sortable: false
        }))
        const tableData = {
            title: 'Game Stats',
            itemKey: undefined
        }

        const spreadsheet = getters.spreadsheetByName(name)
        if (!spreadsheet.elements) {
            return {
                ...tableData,
                headers,
                items: []
            }
        }

        const startingIndex = 8
        const elements = spreadsheet.elements.slice(startingIndex)

        const items = _.map(elements, (el) => ({
            [tableKeys.name]: el[name],
            [tableKeys.game1]: el.Points,
            [tableKeys.game2]: el.Wins,
            [tableKeys.game3]: el.Losses,
            [tableKeys.game4]: el['Missed Weeks'],
            [tableKeys.game5]: el.Played,
            [tableKeys.game6]: el['Points Avg'],
            [tableKeys.game7]: el['Mean Difficulty'],
            [tableKeys.game8]: el.GameX1,
            [tableKeys.game9]: el.GameX2,
            [tableKeys.game10]: el.GameX3,
            [tableKeys.totals]: el.Totals
        }))

        return {
            ...tableData,
            headers,
            items
        }
    }
}

const actions = {
    resetGameStat: ({ commit }) => commit(mutationTypes.RESET_GAMESTAT),
    setGameStatSpreadsheet: ({ commit }, data) => {
        commit(mutationTypes.RESET_SPREADSHEET_ERROR)
        commit(mutationTypes.SET_SPREADSHEET, data)
    },
    setGameStatSpreadsheetLoading: ({ commit }, data = true) => commit(mutationTypes.SET_SPREADSHEET_LOADING, data),
    setGameStatError: ({ commit }, data) => commit(mutationTypes.SET_SPREADSHEET_ERROR, data),
    resetGameStatError: ({ commit }) => commit(mutationTypes.RESET_SPREADSHEET_ERROR)
}

const mutations = {
    [mutationTypes.RESET_GAMESTAT] (state) {
        Object.assign(state, initialState())
    },
    [mutationTypes.SET_SPREADSHEET] (state, data) {
        state.spreadsheet = data
    },
    [mutationTypes.SET_SPREADSHEET_ERROR] (state, data) {
        state.spreadsheetError = data
    },
    [mutationTypes.RESET_SPREADSHEET_ERROR] (state) {
        state.spreadsheetError = initialState().spreadsheetError
    },
    [mutationTypes.SET_SPREADSHEET_LOADING] (state, data) {
        state.spreadsheetLoading = data
    }
}

export default {
    namespaced,
    state,
    actions,
    getters,
    mutations
}
