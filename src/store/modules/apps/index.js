import * as mutationTypes from './mutation-types'
import FPL from './modules/fpl'
import GameStat from './modules/game-stat'

const namespaced = true

const modules = {
    FPL,
    GameStat
}

const initialState = () => ({
})

const state = initialState()

const getters = {
}

const actions = {
}

const mutations = {
}

export default {
    namespaced,
    state,
    actions,
    getters,
    modules,
    mutations
}
