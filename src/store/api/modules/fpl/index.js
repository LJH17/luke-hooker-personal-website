import axios from 'axios'

const proxyUrl = 'https://thingproxy.freeboard.io/fetch/'
const baseUrl = encodeURIComponent('https://fantasy.premierleague.com/api/')
const proxyBaseUrl = `${proxyUrl}${baseUrl}`
const config = {
    headers: {
        'X-Requested-With': 'XMLHttpRequest'
    }
}

const getLeagueData = (id) => {
    return new Promise((resolve, reject) => {
        axios.get(`${proxyBaseUrl}${encodeURIComponent(`leagues-classic/${id}/standings/`)}`, config)
            .then((response) => resolve(response.data))
            .catch((error) => reject(error))
    })
}

export default {
    getLeagueData
}
