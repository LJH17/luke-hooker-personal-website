export const HOME = 'home'
export const ABOUT = 'about'
export const PROJECTS = 'projects'
export const APPS = 'apps'
export const FPL = 'FPL'
export const GAME_STAT_STANDINGS = 'game-stat-standings'
export const GAME_STAT_STATS = 'game-stat-stats'
export const GAME_STAT_MONTH_STATS = 'game-stat-month-stats'
export const GAME_STAT_SCORING = 'game-stat-scoring'
export const PL_STAT_PREDICTIONS = 'pl-stat-predictions'

export default {
    HOME,
    ABOUT,
    PROJECTS,
    APPS,
    FPL,
    GAME_STAT_STANDINGS,
    GAME_STAT_STATS,
    GAME_STAT_MONTH_STATS,
    GAME_STAT_SCORING,
    PL_STAT_PREDICTIONS
}
