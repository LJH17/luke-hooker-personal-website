import Vue from 'vue'
import Router from 'vue-router'
import routeNames from './route-names'
import Home from '../views/Home.vue'

Vue.use(Router)

export default new Router({
    mode: 'history',
    routes: [
        {
            path: '/',
            name: routeNames.HOME,
            component: Home
        },
        {
            path: '/home',
            redirect: '/'
        },
        {
            path: '/about',
            name: routeNames.ABOUT,
            // route level code-splitting
            // this generates a separate chunk (about.[hash].js) for this route
            // which is lazy-loaded when the route is visited.
            component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
        },
        {
            path: '/projects',
            name: routeNames.PROJECTS,
            component: () => import('../views/Projects.vue')
        },
        {
            path: '/apps',
            name: routeNames.APPS,
            redirect: '/apps/fpl',
            component: () => import('../views/Apps.vue'),
            children: [
                {
                    path: 'fpl',
                    name: routeNames.FPL,
                    component: () => import('../views/apps/fpl/FPL.vue')
                },
                {
                    path: 'game-stat',
                    component: () => import('../views/apps/game-stat/GameStat.vue'),
                    children: [
                        {
                            path: '/',
                            name: routeNames.GAME_STAT_STANDINGS,
                            component: () => import('../views/apps/game-stat/tabs/Standings.vue')
                        },
                        {
                            path: 'stats',
                            name: routeNames.GAME_STAT_STATS,
                            component: () => import('../views/apps/game-stat/tabs/Stats.vue')
                        },
                        {
                            path: 'month-stats',
                            name: routeNames.GAME_STAT_MONTH_STATS,
                            component: () => import('../views/apps/game-stat/tabs/MonthStats.vue')
                        },
                        {
                            path: 'scoring',
                            name: routeNames.GAME_STAT_SCORING,
                            component: () => import('../views/apps/game-stat/tabs/Scoring.vue')
                        }
                    ]
                },
                {
                    path: 'pl-stat',
                    name: routeNames.PL_STAT_PREDICTIONS,
                    component: () => import('../views/apps/pl-stat/PLStat.vue')
                }
            ]
        },
        {
            path: '*',
            redirect: '/' // redirect any invalid routes to the home page
        }
    ],
    scrollBehavior (to, from, savedPosition) {
        if (savedPosition) {
            return savedPosition
        } else {
            return { x: 0, y: 0 }
        }
    }
})
