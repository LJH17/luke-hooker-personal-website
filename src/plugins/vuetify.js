import Vue from 'vue'
import Vuetify from 'vuetify/lib'
import 'vuetify/src/stylus/app.styl'
import '@fortawesome/fontawesome-free/css/all.css'
import colors from 'vuetify/es5/util/colors'

Vue.use(Vuetify, {
    theme: {
        primary: colors.orange.base,
        secondary: colors.grey.darken2,
        accent: colors.blue.accent2,
        error: colors.red.accent2,
        info: colors.blue.accent1,
        success: colors.green.base,
        warning: colors.yellow.darken1
    },
    customProperties: true,
    iconfont: 'md'
})
