import moment from 'moment/moment'
import underscore from 'underscore'
import underscoreString from 'underscore.string'

moment.defineLocale('no-time', {
    calendar: {
        sameDay: '[Today]',
        nextDay: '[Tomorrow]',
        nextWeek: 'dddd',
        lastDay: '[Yesterday]',
        lastWeek: '[Last] dddd',
        sameElse: 'DD/MM/YYYY'
    }
})
moment.locale('en-GB')

underscore.mixin(underscoreString.exports())

export default {
    install (Vue) {
        Object.defineProperties(Vue.prototype, {
            $_: { value: underscore },
            $moment: { value: moment }
        })
    }
}
