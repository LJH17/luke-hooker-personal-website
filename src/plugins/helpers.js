import Vue from 'vue'

const helpers = {
    openLink: (url) => {
        window.open(url, '_blank').opener = null
    }
}

const plugin = {
    install () {
        Vue.helpers = helpers
        Vue.prototype.$helpers = helpers
    }
}

Vue.use(plugin)
