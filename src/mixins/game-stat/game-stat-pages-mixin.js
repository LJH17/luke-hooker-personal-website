import { mapGetters } from 'vuex'
import Table from '@/components/layout/Table'

export default {
    components: {
        Table
    },
    computed: {
        ...mapGetters('apps/GameStat', [
            'spreadsheetLoading'
        ]),
        tableData: () => ({
            title: '',
            headers: [],
            items: [],
            itemKey: 'id'
        }),
        tableProps () {
            return {
                actions: false,
                loading: this.spreadsheetLoading,
                ...this.tableData
            }
        }
    }
}
