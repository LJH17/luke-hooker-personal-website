const colors = require('vuetify/es5/util/colors').default
const webpackConfig = require('./webpack.config')

module.exports = {
    runtimeCompiler: true,
    configureWebpack: webpackConfig,
    pwa: {
        name: 'Luke Hooker - Personal Website',
        themeColor: colors.orange.base,
        msTileColor: colors.shades.black,
        appleMobileWebAppCapable: 'yes',
        appleMobileWebAppStatusBarStyle: 'black'
    }
}
