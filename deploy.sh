#!/usr/bin/env bash

#1. Lint and fix project files
yarn lint --fix

#2. Build production files
yarn build

#3. rsync with tsohost to upload any changed files
lftp -e "set ftp:ssl-allow no; mirror -R ./dist/ ./; bye" -u edit@lukehooker.com ftp.lukehooker.com

echo "\nUploaded new dist files"
